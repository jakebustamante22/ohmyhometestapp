package omh.testapplication;

import android.app.Application;

import java.util.Calendar;


import android.app.Application;

import java.util.Calendar;

public class MApplication extends Application {
    public static final String TAG = MApplication.class.getSimpleName();
    private static MApplication sInstance;

    private Calendar startTime, endTime;


    public static MApplication getInstance() {
        return sInstance;
    }

    public void onCreate() {
        sInstance = this;

        super.onCreate();
    }

    public void startActivityTransitionTimer() {
        startTime = Calendar.getInstance();
    }


    public long stopActivityTransitionTimer() {
        endTime = Calendar.getInstance();
        if (startTime != null) {
            return endTime.getTime().getTime() - startTime.getTime().getTime();
        }
        return -1;
    }

}
