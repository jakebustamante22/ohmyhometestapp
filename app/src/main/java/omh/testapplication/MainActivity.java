package omh.testapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import omh.testapplication.kotlin.view.task1.Task1;
import omh.testapplication.kotlin.view.task2.Task2;
import omh.testapplication.kotlin.view.task3.Task3;

public class MainActivity extends AppCompatActivity {

    /* TODO:
    * Task 1) Optimus Prime!
    * Task 2) Photocopy machine!
    * Task 3) StarWars!
    *
    * --- Note ---
    * Do not change the minSdkVersion and make sure all code/layout is backported all the way to the minSdkVersion.
    * */


    public void startTask1(View v) {
        Intent startIntent = new Intent(MainActivity.this, Task1.class);
        MainActivity.this.startActivity(startIntent);
        MApplication.getInstance().startActivityTransitionTimer();
    }

    public void startTask2(View v) {
        Intent startIntent = new Intent(MainActivity.this, Task2.class);
        MainActivity.this.startActivity(startIntent);
        MApplication.getInstance().startActivityTransitionTimer();
    }

    public void startTask3(View v) {
        Intent startIntent = new Intent(MainActivity.this, Task3.class);
        MainActivity.this.startActivity(startIntent);
        MApplication.getInstance().startActivityTransitionTimer();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
