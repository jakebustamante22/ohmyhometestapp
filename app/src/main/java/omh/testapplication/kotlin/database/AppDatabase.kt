package omh.testapplication.kotlin.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import omh.testapplication.kotlin.dao.FavoritesDao
import omh.testapplication.kotlin.dao.PeopleDao
import omh.testapplication.kotlin.domain.Favorites
import omh.testapplication.kotlin.mvp.people.People

@Database(entities = arrayOf(People::class, Favorites::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun peopleDao(): PeopleDao
    abstract fun favoritesDao(): FavoritesDao
    companion object {

        private var INSTANCE: AppDatabase? = null
        fun getAppDatabase(context: Context): AppDatabase? {
            if (AppDatabase.INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    AppDatabase.INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase::class.java, "starwars.db")
                            .allowMainThreadQueries()
                            .build()
                }
            }
            return AppDatabase.INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}
