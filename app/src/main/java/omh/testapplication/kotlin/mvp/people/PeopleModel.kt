package omh.testapplication.kotlin.mvp.people


import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import omh.testapplication.kotlin.database.AppDatabase
import omh.testapplication.kotlin.domain.PeopleList
import omh.testapplication.kotlin.utils.Utils
import omh.testapplication.network.PeopleService
import omh.testapplication.network.StarWarsApi

class PeopleModel(val conts: Context,private val presenter: IPeople.PeoplePresenterImpl) : IPeople.PeopleModelImpl {
    private var database: AppDatabase? = null

    val peopleObservable: Observable<PeopleList>
        get() = StarWarsApi.client!!.create(PeopleService::class.java)
                .peoples
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())


    val peopleObserver: DisposableObserver<PeopleList>
        get() {

            presenter.showProgressBar(true)
            return object : DisposableObserver<PeopleList>() {
                override fun onNext(peopleList: PeopleList) {
                    database!!.peopleDao().insertMultiplePeople(ArrayList(peopleList.peoples))
                    presenter.updateListRecycler(peopleList.peoples)
                }
                override fun onError(e: Throwable) {
                    Log.d(TAG, "Error$e")
                    e.printStackTrace()
                    presenter.showProgressBar(false)
                }

                override fun onComplete() {
                    Log.d(TAG, "Completed")
                    presenter.showProgressBar(false)
                }
            }
        }

    override fun getPeoplesRequest() {
        database = AppDatabase.getAppDatabase(conts)
        val peopleList  =   database!!.peopleDao().all
        if (Utils.isNetworkAvailable(conts)) {
            if(peopleList.size > 1){
                presenter.showProgressBar(false)
                presenter.updateListRecycler(ArrayList(peopleList))
            }else{
                peopleObservable.subscribeWith<DisposableObserver<PeopleList>>(peopleObserver)
            }
        } else{
            if(peopleList.size > 1){
                presenter.showProgressBar(false)
                presenter.updateListRecycler(ArrayList(peopleList))
            }
        }
    }

    companion object {
        private val TAG = "Peoples"
    }
}
