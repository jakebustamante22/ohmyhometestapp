package omh.testapplication.kotlin.mvp.people

import android.content.Context

interface IPeople {

    interface PeopleModelImpl {
        fun getPeoplesRequest()
    }

    interface PeoplePresenterImpl {
        val context: Context
        fun showProgressBar(status: Boolean)
        fun setView(view: PeopleViewImpl)
        fun updateListRecycler(peoples: ArrayList<People>)
        fun getPeoplesRequest()
    }

    interface PeopleViewImpl {
        fun updateListRecycler(peoples: ArrayList<People>)
        fun updateItemRecycler(people: People, name: String)
        fun showProgressBar(visibilidade: Int)

        companion object {
            val People_KEY = "People"
        }
    }
}