package omh.testapplication.kotlin.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_people.view.*
import omh.testapplication.R
import omh.testapplication.kotlin.mvp.people.People
import omh.testapplication.kotlin.utils.Utils
import omh.testapplication.kotlin.view.task3.fragments.PeopleFragment


class PeopleAdapter(val activity: PeopleFragment, var peoples: ArrayList<People>) : RecyclerView.Adapter<PeopleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_people, parent, false)
        val viewHolder = ViewHolder(view)
        parent.setOnClickListener {

        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(peoples[position])
        holder.itemView.next_cv.setOnClickListener {
            val people = peoples[position]
            val name = peoples[position].name
            Log.d("PeopleAdapter", " Jake peoples position : " + people.name)
            activity.updateItemRecycler(people, name!!)
        }
    }

    override fun getItemCount(): Int {
        return peoples.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setData(people: People) {
            itemView.people_title_tv?.setText(people.name)
            Picasso.get().load(Utils.getImagePeople(people.url!!))
                    .into(itemView.people_iv)
        }
    }

    fun getPeoplesList(peoples: ArrayList<People>) {
        this.peoples.clear()
        this.peoples.addAll(peoples)
        this.notifyDataSetChanged()
    }
}
