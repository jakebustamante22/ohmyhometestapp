package omh.testapplication.kotlin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.content_favorites_adapter.view.*
import omh.testapplication.R
import omh.testapplication.kotlin.dao.FavoritesDao
import omh.testapplication.kotlin.domain.Favorites
import org.koin.core.KoinComponent
import org.koin.core.inject

class FavoritesAdapter(val thread: ArrayList<Favorites>, val context: Context) : RecyclerView.Adapter<ViewHolderFavorites>(), KoinComponent {
    private val favoritesDao: FavoritesDao by inject()
    var name: String? = ""
    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderFavorites {
        return ViewHolderFavorites(LayoutInflater.from(context).inflate(R.layout.content_favorites_adapter, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderFavorites, position: Int) {
        name = thread.get(position).name
        holder.tvType?.text = thread.get(position).name

        holder.itemView.content_favorites_adapter_switch.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
//                favoritesDao.insertFavorite(Favorites(name = name.toString()))
            } else {
                favoritesDao.deleteFavoritesByName(thread.get(position).name)
            }
        }
        if (favoritesDao.ifRecordExist(thread.get(position).name)) {
            holder.itemView.content_favorites_adapter_switch.setChecked(true)
        } else {
            holder.itemView.content_favorites_adapter_switch.setChecked(false)
        }
    }

    override fun getItemCount(): Int {
        return thread.size
    }
}

class ViewHolderFavorites(view: View) : RecyclerView.ViewHolder(view) {
    val tvType = view.content_favorites_adapter_tv
}