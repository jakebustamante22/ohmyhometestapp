package omh.testapplication.kotlin.view.task2

data class User(val name: String, val address: String)