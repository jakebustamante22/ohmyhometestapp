package omh.testapplication.kotlin.view.task1

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.content_complex_view.*
import omh.testapplication.KotlinApplication
import omh.testapplication.MApplication
import omh.testapplication.R
import omh.testapplication.kotlin.adapter.Task1ImageAdapter

/* TODO:
    * Task 1) Optimus Prime!
    * --- Description ---
    * Optimize the view layout hierarchy and do whatever you can to make this activity launch faster on the 1st load
    * without changing the layout of the view.
    *
    * Edit activity_task1.xml and content_complex_view.xml
    *
    * */

/**
 * Edited by Jake Clinton Bustamante on 12/06/19.
 *
 */

class Task1 : AppCompatActivity() {

    val testString: ArrayList<String> = ArrayList()
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task1)
        val toolbar = findViewById<Toolbar>(R.id.activity_task1_toolbar)
        setSupportActionBar(toolbar)
        val fab = findViewById<FloatingActionButton>(R.id.activity_task1_fab)
        addString()
        // Creates a vertical Layout Manager
        content_commplex_view_similar_rv.layoutManager = LinearLayoutManager(this,OrientationHelper.HORIZONTAL,false)
        content_commplex_view_similar_rv.adapter = Task1ImageAdapter(testString, this)

        fab.setOnClickListener(View.OnClickListener { view ->
            Snackbar.make(view, "Copyright © 2019 Ohmyhome Pte Ltd, All rights reserved", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        })
    }

    fun addString() {
        testString.add("This is a beautiful house!")
        testString.add("This is a beautiful house!")
        testString.add("This is a beautiful house!")
        testString.add("This is a beautiful house!")
        testString.add("This is a beautiful house!")
        testString.add("This is a beautiful house!")
    }

}