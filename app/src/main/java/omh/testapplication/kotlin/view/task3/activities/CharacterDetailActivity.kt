package omh.testapplication.kotlin.view.task3.activities

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_character.*
import kotlinx.android.synthetic.main.content_character.*
import omh.testapplication.R
import omh.testapplication.kotlin.dao.FavoritesDao
import omh.testapplication.kotlin.domain.Favorites
import omh.testapplication.kotlin.mvp.people.IPeople
import omh.testapplication.kotlin.mvp.people.People
import omh.testapplication.kotlin.utils.Utils
import org.jetbrains.anko.toast
import org.koin.core.KoinComponent
import org.koin.core.inject

class CharacterDetailActivity : AppCompatActivity(), KoinComponent {

    private val favoritesDao: FavoritesDao by inject()

    lateinit var people: People
    var name: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)
        setSupportActionBar(toolbar)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        val bundle = intent

        if (bundle != null) {
            people = intent.getParcelableExtra(IPeople.PeopleViewImpl.People_KEY) as People
            name = intent.getStringExtra("name")
            people_content_birth_year_tv?.text = Utils.fromHtml(getString(R.string.birth_year, people.birth_year))
            people_content_gender_tv?.text = Utils.fromHtml(getString(R.string.gender, people.gender))
            people_content_height_tv?.text = Utils.fromHtml(getString(R.string.height, people.height))
            people_content_hair_color_tv?.text = Utils.fromHtml(getString(R.string.hair_color, people.hair_color))
            people_content_skin_color_tv?.text = Utils.fromHtml(getString(R.string.skin_color, people.skin_color))
            people_content_mass_tv?.text = Utils.fromHtml(getString(R.string.mass, people.mass))
            Picasso.get().load(Utils.getImagePeople(people.url!!)).into(people_detail_iv)
            setTitle(people.name)
        }
        if (favoritesDao.ifRecordExist(name)) {
            people_content_favorite_sw.setChecked(true)
        }else{
            people_content_favorite_sw.setChecked(false)
        }
        onHideAndShowToggle()

    }

    fun onHideAndShowToggle() {
        people_content_favorite_sw.toggle()   //switch state
        people_content_favorite_sw.toggle(false)//switch without animation
        people_content_favorite_sw.setEnableEffect(true)//disable the switch animation
        people_content_favorite_sw.setOnCheckedChangeListener { view, isChecked ->
            Log.d("C", "test1 :  isChecked!" + isChecked)
            if (isChecked) {
                //display toast
                favoritesDao.insertFavorite(Favorites(name = people.name.toString()))
            } else {
                //display dialog
                toast("not Checked")
                favoritesDao.deleteFavoritesByName(name.toString())
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
