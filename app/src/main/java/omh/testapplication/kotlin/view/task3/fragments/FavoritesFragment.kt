package omh.testapplication.kotlin.view.task3.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_favorites.*
import omh.testapplication.R
import omh.testapplication.kotlin.adapter.FavoritesAdapter
import omh.testapplication.kotlin.dao.FavoritesDao
import omh.testapplication.kotlin.domain.Favorites
import org.koin.core.KoinComponent
import org.koin.core.inject

/**
 * Edited by Jake Clinton Bustamante on 12/11/19.
 *
 */

class FavoritesFragment() : Fragment(), KoinComponent {

    lateinit var inflate: View
    private val favoritesDao: FavoritesDao by inject()
    private var threadItem: ArrayList<Favorites> = ArrayList()
    private var threadListAdapter: FavoritesAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        inflate = inflater.inflate(R.layout.fragment_favorites, container, false)
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //get data from favorites
        threadListAdapter = FavoritesAdapter(threadItem, context!!)
        favorites_rv.apply {
            layoutManager = LinearLayoutManager(view?.context, RecyclerView.VERTICAL, false)
            adapter = threadListAdapter
        }
        favoritesDao.getAll().observe(this, Observer { newThread ->
            threadItem.let { threadList ->
                threadList.clear()
                threadList.addAll(newThread as Collection<Favorites>)
                threadListAdapter!!.notifyDataSetChanged()
            }
        })

    }
}