package omh.testapplication.kotlin.view.task2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_task2.*
import omh.testapplication.R
import omh.testapplication.kotlin.TestMainActivity
import omh.testapplication.kotlin.adapter.AppointmentAdapter
import omh.testapplication.kotlin.view.task1.Task1
import omh.testapplication.kotlin.view.task3.Task3


/* TODO:
    * Task 2) Photocopy machine!
    *
    *  --- Description ---
    *  Study the image shown in the activity, task2.png
    *  and replicate the design into layouts and views.
    *  There is no restriction on how to replicate the design use your
    *  creativity :)
    *
    *  --- Requirements ---
    * - Left/Right swipeable calendar
    * - Bottom slideable overlay + expandable listview
    * - Follow the UI design as closely as possible
    * - Naming conventions have to be clear and readable
    * - Code has to be neat and organize
    * - Create any custom files/classes you want, edit file activity_task2.xml
    * - Any missing assets please get from https://icons8.com/
    * - Incorrect icons/ images will not be penalised
    *
    * --- References ---
    * - Google Calendar
    *
    * --- Plus Points ---
    * - Good UX
    * - Using newer layouts, i.e. ConstraintLayout, CoordinatorLayout
    *
    * */


class Task2 : AppCompatActivity(), View.OnClickListener {

    val users = ArrayList<User>()

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task2)

        //getting recyclerview from xml
        val recyclerView = findViewById(R.id.recyclerView) as RecyclerView
        //adding a layoutmanager
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val adapter = AppointmentAdapter(users)
        back_iv.setOnClickListener(this)
        //crating an arraylist to store users using the data class user

        users.add(User("Viewing Appointment", "Jake set an appointment\n to view your unit"))
        users.add(User("Pending Appointment", "Tom set an appointment\n to view your unit"))
        users.add(User("Test Test", "Test test"))
        users.add(User("Test Test", "Test test"))

        //crating an arraylist to store users using the data class user
        val users = ArrayList<User>()
        recyclerView.adapter = adapter

//        val toolbar = activity_task2_toolbar
//        activity_task2_toolbar.title = "Back"
////        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
////        supportActionBar!!.setDisplayShowHomeEnabled(true);
//        activity_task2_toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp)
//        activity_task2_toolbar.setNavigationOnClickListener {
////            activity_task2_toolbar.navigationIcon = null
////            activity_task2_toolbar.title = null
//            val intent = Intent(this@Task2,TestMainActivity::class.java)
//            startActivity(intent)
//        }

    }

    override fun onClick(v: View?) {
        if (v == back_iv) {
            startActivity(Intent(this@Task2, TestMainActivity::class.java))
        }
    }


}