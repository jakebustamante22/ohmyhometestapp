package omh.testapplication.kotlin.view.task3.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_people.view.*
import omh.testapplication.R
import omh.testapplication.kotlin.adapter.PeopleAdapter
import omh.testapplication.kotlin.mvp.people.IPeople
import omh.testapplication.kotlin.mvp.people.People
import omh.testapplication.kotlin.mvp.people.PeoplePresenter
import omh.testapplication.kotlin.utils.InternetFragment
import omh.testapplication.kotlin.view.task3.activities.CharacterDetailActivity


/**
 * Edited by Jake Clinton Bustamante on 12/10/19.
 *
 */
class PeopleFragment :  InternetFragment(), IPeople.PeopleViewImpl, SwipeRefreshLayout.OnRefreshListener {

    private var presenter: IPeople.PeoplePresenterImpl? = null
    private var PeopleAdapter: PeopleAdapter? = null
    lateinit var inflate : View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        inflate = inflater.inflate(R.layout.fragment_people, container, false)
        if (presenter == null) {
            presenter = PeoplePresenter(context!!)
        }
        presenter!!.setView(this)
        inflate.people_sr.setOnRefreshListener(this)
        inflate.people_rv.layoutManager = GridLayoutManager(activity, 2) as RecyclerView.LayoutManager?
        inflate.people_rv.itemAnimator = DefaultItemAnimator()
        inflate.people_rv.setHasFixedSize(true)
        PeopleAdapter = PeopleAdapter(this ,arrayListOf<People>() )
        inflate.people_rv.adapter = PeopleAdapter
        presenter!!.getPeoplesRequest()
        return inflate
    }

    override fun updateItemRecycler(people: People, name: String) {
           val intent = Intent(activity, CharacterDetailActivity::class.java)
           intent.putExtra(IPeople.PeopleViewImpl.People_KEY, people)
           intent.putExtra("name", name)
           startActivity(intent)
    }

    override fun updateListRecycler(peoples: ArrayList<People>) {
        PeopleAdapter!!.getPeoplesList(peoples)
        PeopleAdapter!!.notifyDataSetChanged()
    }

    override fun showProgressBar(visibility: Int) {
        if (inflate.people_sr?.isRefreshing!!) {
            inflate.people_sr?.isRefreshing = false
        } else if (visibility == 8) {
            inflate.loading_pb.setVisibility(View.GONE)
        } else {
            inflate.loading_pb.setVisibility(visibility)

        }
    }

    override fun onRefresh() {
        presenter?.getPeoplesRequest()
    }


    override fun initialDownload() {
        presenter?.getPeoplesRequest()
    }


}
