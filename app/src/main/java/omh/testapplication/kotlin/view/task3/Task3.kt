package omh.testapplication.kotlin.view.task3

import omh.testapplication.R

import android.os.Bundle
import android.util.Log

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_task3.*
import omh.testapplication.kotlin.view.task3.fragments.FavoritesFragment
import omh.testapplication.kotlin.view.task3.fragments.PeopleFragment

/* TODO:
    * Task 3) StarWars!
    *  --- Description ---
    *  This tasks requires you to make api calls and understand JSON serialization.
    *  You will be working with public apis and handling local data storage as well.
    *  Create 2 seperate tabs for this task.
    *  Tab 1:
    *       - Display List of Characters
    *       - Enable search function to search specific/ filter  Characters
    *       - Able to favourite/unfavourite any Character
    *  Tab 2:
    *       - Show favourited Characters
    *       - Able to unfavourite Characters
    *
    *  --- Requirements ---
    *  - Make use of the api https://swapi.co/api/people/ to get the list of characters.
    *       - Response will be paginated, so only load results on demand.
    *  - Make use of the api https://swapi.co/api/people/?search={val} to search for characters
    *       - Replace {val} with any searcheable value e.g. luke
    *  - Favourite/Unfavourite any star wars character
    *  - Favourited characters will be stored locally
    *  - Characters Required field to display: name, gender, height, mass, birthday
    *
    *  - Naming conventions have to be clear and readable
    *  - Code has to be neat and organize
    *  - Create any custom files/classes you want, edit file activity_task3.xml
    *  - UI/UX will not be penalized
    *
    *  Preferred libraries to work with - retrofit, realm
    *
    *  --- References ---
    *  API host endpoint : https://swapi.co/api/
    *  Test apis @ https://swapi.co/
    *  Documentation @ https://swapi.co/documentation
    *
    * */

/**
 * Edited by Jake Clinton Bustamante on 12/09/19.
 *
 */

class Task3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task3)
        people_tav_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        loadFragment(PeopleFragment())
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_favorites -> {
                Log.d("t3"," favortissss")
                loadFragment(FavoritesFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_characters -> {
                loadFragment(PeopleFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    fun loadFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        fragmentManager.apply {
            val transaction = this.beginTransaction()
            transaction.replace(R.id.container, fragment)
            transaction.commit()
        }
    }

}
