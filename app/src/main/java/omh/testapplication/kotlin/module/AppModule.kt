package omh.testapplication.kotlin.module

import android.content.Context
import omh.testapplication.kotlin.dao.FavoritesDao
import omh.testapplication.kotlin.database.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val applicationModule = module {
    factory { createMyFavoritesDao(androidContext()) }
}

fun createMyFavoritesDao(androidContext: Context): FavoritesDao {
    return AppDatabase.getAppDatabase(androidContext)!!.favoritesDao()
}
