package omh.testapplication.kotlin.domain

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import omh.testapplication.kotlin.mvp.people.People

data class PeopleList (@Expose
                       @SerializedName("count")
                       val count: String,
                       @Expose
                       @SerializedName("next")
                       val next: String,
                       @Expose
                       @SerializedName("previous")
                       val previous: String,
                       @Expose
                       @SerializedName("results")
                       var peoples: ArrayList<People>)