package omh.testapplication.kotlin.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "Favorites")
data class Favorites(
        @PrimaryKey(autoGenerate = true) var id: Int? = null,
        @SerializedName("name") var name: String)

