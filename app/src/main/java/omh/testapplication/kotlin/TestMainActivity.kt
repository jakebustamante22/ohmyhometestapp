package omh.testapplication.kotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import omh.testapplication.R
import omh.testapplication.kotlin.view.task1.Task1
import omh.testapplication.kotlin.view.task2.Task2
import omh.testapplication.kotlin.view.task3.Task3

/**
 * Created by Jake Clinton Bustamante on 12/06/19.
 */

class TestMainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        task1_tv.setOnClickListener(this)
        task2_tv.setOnClickListener(this)
        task3_tv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v == task1_tv) {
            startActivity(Intent(this@TestMainActivity, Task1::class.java))
//            Toast.makeText(applicationContext, "this is toast message1", Toast.LENGTH_SHORT).show()
        } else if (v == task2_tv) {
//            Toast.makeText(applicationContext, "this is toast message2", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@TestMainActivity, Task2::class.java))
        } else if (v == task3_tv) {
            startActivity(Intent(this@TestMainActivity, Task3::class.java))
//            Toast.makeText(applicationContext, "this is toast message3", Toast.LENGTH_SHORT).show()
        }
    }
}
