package omh.testapplication.kotlin.Extensions

fun String.removeSpecialChar(): String {
    val re = Regex("[^A-Za-z0-9 ]")
    return re.replace(this, "")
}

fun String.intOrString(): Any {
    val v = toIntOrNull()
    return when(v) {
        null -> this
        else -> v
    }
}