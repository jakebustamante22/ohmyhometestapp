@file:Suppress("DEPRECATION")

package omh.testapplication.kotlin.utils

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.Toast
import omh.testapplication.R

object Utils {

    fun getImagePeople(url: String): String {
        return "https://starwars-visualguide.com/assets/img/" + spliPeopleString(url) + ".jpg"
    }

    fun spliPeopleString(s: String): String {
        val separated = s.split("/")
        return "characters/" + separated[separated.size - 2]
    }

    fun isNetworkAvailable(mContext: Context): Boolean {
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return if (netInfo != null && netInfo.isConnectedOrConnecting) {
            true
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.test), Toast.LENGTH_LONG).show()

            false
        }
    }

    @SuppressWarnings("deprecation")
    fun fromHtml(source: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(source)
        }
    }
}