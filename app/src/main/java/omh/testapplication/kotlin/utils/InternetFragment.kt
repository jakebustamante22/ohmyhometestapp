package omh.testapplication.kotlin.utils


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.fragment.app.Fragment
import omh.testapplication.kotlin.utils.Utils.isNetworkAvailable


@Suppress("DEPRECATION")
abstract class InternetFragment : Fragment() {

    private var mReceiver: ConnectionReciever? = null

     override fun onResume() {
        super.onResume()
        mReceiver = ConnectionReciever()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
         activity?.registerReceiver(mReceiver, filter)
    }

     override fun onPause() {
        super.onPause()
        activity?.unregisterReceiver(mReceiver)
    }

    abstract fun initialDownload()

    internal inner class ConnectionReciever : BroadcastReceiver() {

        var mFirst = true

        override fun onReceive(context: Context, intent: Intent) {

            if (mFirst) {
                mFirst = false
                return
            }

            if (ConnectivityManager.CONNECTIVITY_ACTION == intent.action) {
                if (isNetworkAvailable(context)) {
                    initialDownload()
                }
            }
        }
    }

    inner class NetworkStateReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("app", "Network connectivity change")
            if (intent.extras != null) {
                val ni = intent.extras!!.get(ConnectivityManager.EXTRA_NETWORK_INFO) as NetworkInfo
                if (ni.state == NetworkInfo.State.CONNECTED) {
                    Log.i("app", "Network " + ni.typeName + " connected")
                } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, java.lang.Boolean.FALSE)) {
                    Log.d("app", "There's no network connectivity")
                }
            }
        }
    }
}

