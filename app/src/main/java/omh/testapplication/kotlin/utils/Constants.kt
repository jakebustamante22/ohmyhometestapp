package omh.testapplication.kotlin.utils

object Constants {
    //base URL
    const val PREF_NAME = "JunyverseCache"
    const val BASE_URL = "https://swapi.co/api/"
    const val REQUEST_OKHTTP_TIMEOUT = 10L
}