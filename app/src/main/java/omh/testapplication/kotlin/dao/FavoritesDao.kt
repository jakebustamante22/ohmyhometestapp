package omh.testapplication.kotlin.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import omh.testapplication.kotlin.domain.Favorites

const val favoritesTable = "Favorites"
@Dao
interface FavoritesDao {

    @Query("SELECT * FROM $favoritesTable")
    fun getAll(): LiveData<List<Favorites>>

    @Query("SELECT * FROM $favoritesTable WHERE name = :name")
    fun ifRecordExist(name: String?): Boolean


    @Query("DELETE FROM $favoritesTable WHERE name = :name")
    fun deleteFavoritesByName(name: String)

    @Insert
    fun insertFavorite(favorites: Favorites)


    @Update
    fun updateOnlySinglePeople(favorites: Favorites)

    @Delete
    fun deleteOnlySinglePeople(favorites: Favorites)


}