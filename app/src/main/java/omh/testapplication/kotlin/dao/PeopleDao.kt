package omh.testapplication.kotlin.dao

import androidx.room.*
import omh.testapplication.kotlin.mvp.people.People


@Dao
interface PeopleDao {

    @get:Query("SELECT * FROM people")
    val all: List<People>

    @Insert
    fun insertOnlySinglePeople(people: People)

    @Insert
    fun insertMultiplePeople(people: List<People>)

    @Update
    fun updatePeople(people: People)

    @Delete
    fun deletePeople(people: People)
}