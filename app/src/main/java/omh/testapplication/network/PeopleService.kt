package omh.testapplication.network

import io.reactivex.Observable
import omh.testapplication.kotlin.domain.PeopleList
import retrofit2.http.GET


interface PeopleService {

    @get:GET("people")
    val peoples: Observable<PeopleList>


}
