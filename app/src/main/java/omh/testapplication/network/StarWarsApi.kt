package omh.testapplication.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import omh.testapplication.kotlin.utils.Constants.BASE_URL
import omh.testapplication.kotlin.utils.Constants.REQUEST_OKHTTP_TIMEOUT
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object StarWarsApi {

    private var retrofit: Retrofit? = null

    val client: Retrofit?
        get() {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                    .connectTimeout(REQUEST_OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(REQUEST_OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(REQUEST_OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(interceptor).build()

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build()
            }

            return retrofit
        }
}