package omh.testapplication

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import omh.testapplication.kotlin.module.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.util.*


class KotlinApplication : Application() {

     var startTime: Calendar? = null
     var endTime:Calendar? = null

    companion object {
        private var instance: KotlinApplication? = null;
        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        // start Koin!
        startKoin {
            // Android context
            androidContext(this@KotlinApplication)
            // modules
            modules(applicationModule)
        }
    }

    fun stopActivityTransitionTimer(): Long {
        endTime = Calendar.getInstance()
        return if (startTime != null) {
            endTime!!.getTime().getTime() - startTime!!.getTime().getTime()
        } else -1
    }
}