package omh.testapplication.application

import android.content.Context
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import org.jetbrains.anko.doAsync

class TestApplication : DaggerApplication() {

    companion object {
        private var instance: TestApplication? = null;

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    init {
        instance = this
    }
    override fun onCreate() {
        super.onCreate()
        //For Db viewer in Chrome
        Stetho.initializeWithDefaults(this)
    }

    @Synchronized
    fun getInstance() = instance

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}