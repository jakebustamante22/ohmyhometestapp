# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This test application consists of 3 task.
1) Optimization
2) Following design
3) Network and local storage

The instructions of individual task are found at the top of the 3 activities:
Task1.java
Task2.java
Task3.java

or if you are familiar with Kotlin you can work on:
kotlin/Task1.kt
kotlin/Task2.kt
kotlin/Task3.kt

Please read properly and perform them to your best abilities.

This codebase is only distributed to authorize personnel.
Any redistribution or sharing of codebase is strictly prohibited.

### How do I get set up? ###
This code is written in Android Studio 3.4.1 and it is recommended to be open with Android Studio.
Run the code onto an android device/ emulator.

### Contribution guidelines ###
Please fork from this repository and not to commit directly to the master branch.
Make your repository private and invite kevin@ohmyhome.sg to it.

### Who do I talk to? ###
Any issues you face along the way please contact me @ kevin@ohmyhome.sg